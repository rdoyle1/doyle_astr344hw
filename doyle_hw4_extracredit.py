import numpy as np
import math
import random
import threading

class ec:
	def __init__(self):
		self.numsteps=100
		self.f=None
		self.desc=None
	
	def run(self,function,dsc,start=-1.,end=1.):
		self.f=function
		self.desc=dsc
		self.x=np.linspace(start,end,self.numsteps).tolist()
		self.y=map(self.f,self.x)
		linval=self.lin(self.y,self.x)
		midval=self.mid(self.x)
		trapval=self.trap(self.y,self.x)
		simval=self.simp(self.y,self.x)
		gauval=self.gaus(self.y,self.x)
		print("Function: {}".format(self.desc))
		print("Piecewise linear (Left endpoint): {}, Error: {}%".format(linval,np.abs((simval-linval)/simval)*100))
		print("Midpoint: {}, Error: {}%".format(midval,np.abs((simval-midval)/simval)*100))
		print("Trapezoidal: {}, Error: {}%".format(trapval,np.abs((simval-trapval)/simval)*100))
		print("Simpson's: {}, Error: {}%".format(simval,np.abs((simval-simval)/simval)*100))
		print("Gaussian: {}, Error: {}%".format(gauval,np.abs((simval-gauval)/simval)*100))
		#print("Monte Carlo: {}\n".format(self.monte(self.y,self.x)))

	"""
	Calculates the integral using either left endpoint (left=True) or right endpoint (left=False) method
	"""
	def lin(self, y, x=None, dx=1, left=True):
		y=np.asarray(y,dtype=np.float)
		if x==None:
			x=np.linspace(0,dx*len(y)-1,len(y))
		else:
			x=np.asarray(x,dtype=np.float)
		arr=[]
		for i in range(len(x)):
			if left:
				if i<len(x)-1:
					arr.append(y[i]*(x[i+1]-x[i]))
			else:
				if i>0:
					arr.append(y[i]*(x[i]-x[i-1]))
		return np.sum(arr)

	"""
	Calculates integral using the midpoint formula
	"""
	def mid(self, x):
		arr=[]
		for i in range(len(x)):
			if i<len(x)-1:
				arr.append((x[i+1]-x[i])*(self.f((x[i+1]+x[i])/2.)))
		return np.sum(arr)

	"""
	Calculates integral using the trapezoidal rule
	"""
	def trap(self, y, x=None, dx=1):
                y=np.asarray(y,dtype=np.float)
                if x==None:
                        x=np.linspace(0,dx*len(y)-1,len(y))
                else:
                        x=np.asarray(x,dtype=np.float)
		arr=[]
                for i in range(len(x)):
			if i<len(x)-1:
				arr.append(((y[i+1]+y[i])/2)*(x[i+1]-x[i]))
		return np.sum(arr)

	"""
	Calculates the integral using Simpson's rule. Essentially, using a weighted average
	of the midpoint and trapezoidal formula, since both end up overestimating/underestimating
	the result by a predictable and consistent amount, we can get an exact value for low-degree polynomials.
	"""
	def simp(self,y,x=None,dx=1):
		M=self.mid(x)
		T=self.trap(y,x)
		return (2.*M+T)/3.

	"""
	I don't exactly know if I did this one right, but basically, using the weighting function
	(We use equal weights of (b-a)/2), we can get an exact value for low order polynomials.
	I tried this on a higher order one, and it didn't work at all.
	"""
	def gaus(self,y,x=None,dx=1):
		a=-1.
		b=1.
		c1=(b-a)/2.
		c2=(b-a)/2.
		x1=((b-a)/2.)*(-1./np.sqrt(3.))+((b+a)/2.)
		x2=((b-a)/2.)*(1./np.sqrt(3.))+((b+a)/2.)
		return c1*self.f(x1)+c2*self.f(x2)

	"""
	Just for fun, I did a monte-carlo integration method. It works okay, but part of the problem is
	that random number generators are surprisingly non-random, and as a result, more of the numbers
	will be selected nearer the center of the box, rather than the edge. This can lead to incorrect
	assumptions about the function near the edge, since there are fewer sample points. I tried
	to mitigate this with a large number of points, but it takes a long time to run.
	"""
	def monte(self,y,x,numcounts=10000000):
		random.seed()
		randx=randy=0
		min_x=min(x)
		max_x=max(x)
		min_y=min(0.,min(y))
		max_y=max(0.,max(y))
		area=(max_y-min_y)*(max_x-min_x)
		num_pos=0
		num_neg=0
		for i in range(numcounts):
			rand_x=random.uniform(min_x,max_x)
			rand_y=random.uniform(min_y,max_y)
			f_x=self.f(rand_x)
			if f_x>0  and 0<=rand_y<=f_x:
				num_pos+=1
			elif f_x<0 and 0>=rand_y>=f_x:
				num_neg+=1
		pos_fraction=(area*num_pos)/numcounts
		neg_fraction=(area*num_neg)/numcounts
		return pos_fraction-neg_fraction


def f(x):
	return (x**3)+2*(x**2)-4

def f1(x):
	return (x**2)+3*x-1

def f2(x):
	return x**5-3*x**4+x**3-5*x**2+2

test = ec()
print("See comments in code for a bit more info, and other things that I implemented, like a monte-carlo integration method.")
test.run(f,"x^3+2x^2-4")
#test.run(f1,"x^2+3x-1")
#test.run(f2,"x^5-3x^4+x^3-5x^2+2")
