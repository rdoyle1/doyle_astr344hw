#!/usr/bin/python
from datetime import datetime
from datetime import timedelta
from doyle_hw5 import hw5
from scipy import optimize
import numpy as np
import random

def f(x):
	return np.sin(x)

def df(x):
	return np.cos(x)

def B_v(T):
	return (((2*6.626e-27*((2.998e10/0.087)**3))/(2.998e10**2))/(np.exp((6.626e-27*(2.998e10/0.087))/(1.381e-16*T))-1))-1.25e-12

class nr:
	def __init__(self,function=None,df=None,guess=0,tolerance=1.48e-8,iterations=50,sec_vs_der=True):
		self.resetvals=(function,df,guess,tolerance,iterations,sec_vs_der)
		self.x=guess
		self.sec=sec_vs_der
		self.val=0
		self.tolerance=tolerance
		self.iterations=iterations
		self.values=[None]*iterations
		self.values[0]=guess
		self.values[1]=guess+tolerance
		if function==None:
			self.f=self.__func
		else:
			self.f=function
		if df==None:
			self.df=None
		else:
			self.df=df

	def reset(self):
		f,df,self.x,self.tolerance,self.iterations=self.resetvals
		self.values=[None]*iterations
		self.values[0]=guess
		self.values[1]=guess+tolerance

	def set_tolerance(self,tolerance):
		self.tolerance=tolerance

	def __secant(self,x,n):
		if n>1:
			self.values[n]=(self.values[n-2]*self.f(self.values[n-1])-self.values[n-1]*self.f(self.values[n-2]))/(self.f(self.values[n-1])-self.f(self.values[n-2]))
		return self.values[n]

	def __derivative(self,x):
		df1=(self.f(x+(1e-5))-self.f(x))/(1e-5)
		df2=(self.f(x)-self.f(x-(1e-5)))/(1e-5)
		return (df1+df2)/2.

	def __func(self,x):
		return 0

	def find_root(self):
		self.val=self.f(self.x)
		if self.df==None and not self.sec:
			self.df=self.__derivative
		if self.df!=None:
			if np.abs(self.val/self.df(self.x))>1000:
				self.x+=random.uniform(-1,1)
				self.x*=random.uniform(0.9,1.1)
		count=0
		initial=True
		while np.abs(self.val)>self.tolerance and count<self.iterations:
			if self.df==None:
				self.x=self.__secant(self.x,count)
			else:
				dy=self.df(self.x)
				self.x-=(self.val/dy)
			self.val=self.f(self.x)
			count+=1
		return self.x

def compare(t1,d1,t2,d2):
	if t1<t2:
		print("Our {} method ran in {} seconds, and was faster than our {} method, which ran in {} seconds. The time difference was {} seconds. (About {} times faster)".format(d1,t1,d2,t2,t2-t1,t2.total_seconds()/t1.total_seconds()))
	elif t1>t2:
		print("Our {} method ran in {} seconds, and was faster than our {} method, which ran in {} seconds. The time difference was {} seconds. (About {} times faster)".format(d2,t2,d1,t1,t1-t2,t1.total_seconds()/t2.total_seconds()))
	else:
		print("Our {} method and our {} method were equal, and both ran in {} seconds.".format(d2,d1,t1))

if __name__=="__main__":
	print("\nThis imports my homework 5 python program as a module, so that needs to be in the same folder as this.\nEverything is a class, so it's all pretty self-contained. Note that NR uses the derivative method, and NR-Secant\nis an alternate method for calculating our nr roots if the derivative function is not known.\nI will test the NR method against the NR-Secant method and the Bisection method, along with scipy's versions of them.\n")
	print("Part 1: Comparing root finding. Using the function f(x) = sin(x) (root should be x = pi)\n")
	test1=nr(f,df,2)
	t1=datetime.now()
	print("NR: x = {}".format(test1.find_root()))
	t2=datetime.now()
	nrd=t2-t1
	print("Time: {}s\n".format(t2-t1))

	test1=nr(f,None,2)
	t1=datetime.now()
	print("NR-Secant: x = {}".format(test1.find_root()))
	t2=datetime.now()
	nrsd=t2-t1
	print("Time: {}s\n".format(t2-t1))

	test2=hw5(f,[2,4])
	t1=datetime.now()
	print("Bisection: x = {}".format(test2.find_root(limit=False,print_steps=False)))
	t2=datetime.now()
	bisd=t2-t1
	print("Time: {}s\n".format(t2-t1))

	compare(nrd,"NR",nrsd,"NR-Secant")
	compare(nrd,"NR",bisd,"Bisection")
	compare(nrsd,"NR-Secant",bisd,"Bisection")

	t1=datetime.now()
	print("\nScipy NR: x = {}".format(optimize.newton(f,2,fprime=df)))
	t2=datetime.now()
	snrd=t2-t1
	print("Time: {}s\n".format(t2-t1))

	t1=datetime.now()
	print("Scipy NR-Secant: x = {}".format(optimize.newton(f,2)))
	t2=datetime.now()
	snrsd=t2-t1
	print("Time: {}s\n".format(t2-t1))

	t1=datetime.now()
	print("Scipy Bisect: x = {}".format(optimize.bisect(f,2,4)))
	t2=datetime.now()
	sbisd=t2-t1
	print("Time: {}s".format(t2-t1))

	compare(snrd,"Scipy NR",nrsd,"Scipy NR-Secant")
	compare(snrd,"Scipy NR",bisd,"Scipy Bisection")
	compare(snrsd,"Scipy NR-Secant",bisd,"Scipy Bisection")

	test1=nr(B_v,None,50)
	test1.set_tolerance(1e-16)
	print("\nPart 2:")
	print("Secant Method: B_v(T)==1.25e-12 at {}K".format(test1.find_root()))

	test1=nr(B_v,None,50,sec_vs_der=False)
	test1.set_tolerance(1e-16)
	print("Derivative Method: B_v(T)==1.25e-12 at {}K".format(test1.find_root()))
	
	test2=hw5(B_v,[40,45])
	test2.change_tolerance(1e-16)
	print("Bisect Method: B_v(T)==1.25e-12 at {}K".format(test2.find_root(limit=False,print_steps=False)))

	print("Scipy Bisect: B_v(T)==1.25e-12 at {}K".format(optimize.bisect(B_v,40,45)))
	print("Scipy Newton: B_v(T)==1.25e-12 at {}K".format(optimize.newton(B_v,40)))

	print("\nEverything seems to be showing the answer is about 42K. The answer to life, the universe, and everything.\n")
