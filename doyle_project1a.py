import sys
import numpy as np
import time
import random
import matplotlib.pyplot as plt

class proj1:
	"""
	create a project instance. devault values for all parameters are specified
	xdim - x dimension, default 100
	ydim - y dimension, default 100
	num_people - number of people on the board, default 100
	vel - maximum velocity for a person, default 2. They can move slower, though.
	"""
	def __init__(self,xdim=100,ydim=100,num_people=100,vel=2):
		self.x=xdim
		self.y=ydim
		self.num_people=num_people
		self.max_vel=vel
		self.grid = [[-1 for x in range(self.x)] for y in range(self.y)] # initialize grid
		self.people=[] # array of people objects
		for i in range(num_people): # find a place for everyone on the board
			while True:
				xloc=int(np.random.random()*xdim)
				yloc=int(np.random.random()*ydim)
				if self.grid[yloc][xloc]==-1:
					self.grid[yloc][xloc]=i
					self.people.append(student(xloc,yloc,i,vel,self.people,xdim,ydim))
					break

		self.time=0
		self.has_run=False
	
	"""
	updates the board.
	steps - number of time steps to process.
	"""
	def update(self,steps=1):
		for step in range(steps):
			self.temp_grid = [[-1 for x in range(self.x)] for y in range(self.y)] # make a temporary grid
			random.shuffle(self.people) # randomize the order people move in
			for person in self.people:
				person.update() # let the person update their location
				x,y=person.get_pos() # get their new position
				id_num=person.get_id() # get their id number
				self.temp_grid[y][x]=id_num # set the new position on the board
			self.grid=self.temp_grid # save the new grid

	"""
	Lots of ANSI escape sequences. Prints the game board as it currently stands.
	"""
	def print_board(self):
		people=[x.get_id() for x in self.people]
		print("\033[0;0H\033[2J"+"-"*(self.x*3+1))
		for i,row in enumerate(self.grid):
			col=2
			print("\033[{};0H|".format(i+2))
			for person in row:
				if person==-1:
					print("\033[{};{}H   ".format(i+2,col))
				else:
					print("\033[{};{}H * ".format(i+2,col))
				col+=3
			print("\033[{};{}H|".format(i+2,col))
		print("\033[{};0H".format(self.x+2)+"-"*(self.x*3+1))


"""
single person on the board
"""
class student:
	"""
	x,y - the x and y positions of the person
	id_num - the person's id
	max_vel - the maximum velocity they can move
	room - the list of people in the room
	max_x,max_y - the game board dimensions
	"""
	def __init__(self,x,y,id_num,max_vel,room,max_x,max_y):
		self.x=x
		self.y=y
		self.id_num=id_num
		self.room=room
		self.max_vel=max_vel
		self.max_x=max_x
		self.max_y=max_y

	"""
	update the person's position by moving a random direction and velocity
	"""
	def update(self):
		#fvel=(np.log(np.random.random()+1)/np.log(self.max_vel+1))*self.max_vel
		#vel=int(round(fvel,0))
		vel = self.max_vel
		positions = [(x,y) for x in range(self.x-vel,self.x+vel+1) for y in range(self.y-vel,self.y+vel+1) if x>=0 and x<self.max_x and y>=0 and y<self.max_y]
		while True:
			x,y=random.choice(positions)
			if not self.__conflicts(x,y):
				self.x=x
				self.y=y
				break

	"""
	We don't want the positions we choose for our update to conflict with a person's current location
	"""
	def __conflicts(self,x,y):
		for person in self.room:
			if person.get_id()!=self.id_num:
				xpos,ypos=person.get_pos()
				if x==xpos and y==ypos:
					return True
		return False

	"""
	return the position
	"""
	def get_pos(self):
		return self.x,self.y

	"""
	return the id
	"""
	def get_id(self):
		return self.id_num

	"""
	draw from a random exponential
	"""
	def __rand_exp(self,scale=1.0):
		return -scale*(np.log(scale*np.random.random())-np.log(scale))

"""
A menu system I worked up. THis is just to ask if you want to see the simulation run
"""
class menu:
	def query_options(self,question, valid, options=None, default=None):
		optns=options[:]
		opt=""
		if options!=None:
			for i,opt in enumerate(options):
				if default!=None and opt==default:
					optns[i]=optns[i].upper()
			opt="/".join(optns)
		else:
			if default==None:
				opt="no default"
		while 1:
			try:
				choice = raw_input("{} [{}]: ".format(question,opt)).strip().lower()
			except NameError:
				choice = input("{} [{}]: ".format(question,opt)).strip().lower()
			if default is not None and choice is "":
				return valid[default]
			elif choice in valid:
				return valid[choice]
			print("Option {} not recognized...".format(choice))

	def is_byte(self,val):
		try:
			val = int(val)
		except ValueError:
			return False,0
		if val<256:
			return True,val
		else:
			return False,val

	def is_int(self,val):
		try:
			val = int(val)
		except ValueError:
			return False,0
		return True,val

	def is_float(self,val):
		try:
			val = float(val)
		except ValueError:
			return False,0
		return True,val

	def query_int(self,question, default=None):
		dflt = "no default"
		if default is not None:
			valid,val = is_int(default)
			if not valid:
				dflt = 0
			else:
				dflt = default
		while 1:
			try:
				choice = raw_input("{} [{}]: ".format(question,dflt)).strip()
			except NameError:
				choice = input("{} [{}]: ".format(question,dflt)).strip()
			if default is not None and choice is "":
				return default
			valid,val = is_int(choice)
			if valid:
				return val
			print("Argument must be integer...")

	def query_float(self,question, default=None):
	        dflt = "no default"
		if default is not None:
			valid,val = is_float(default)
			if not valid:
				dflt = 0
			else:   
				dflt = default
		while 1:
                	try:
                	        choice = raw_input("{} [{}]: ".format(question,dflt)).strip()
                	except NameError:
                	        choice = input("{} [{}]: ".format(question,dflt)).strip()
                	if default is not None and choice is "":
                	        return default
                	valid,val = is_float(choice)
                	if valid:
                	        return val
                	print("Argument must be integer...")


def main():
	"""
	xdim - board x dimension
	ydim - board y dimension
	nump - number of people on the board
	it - number of iterations of the update function to run
	"""
	print("The first project check in requires 3 things. First, the 2D room.\n\nThe creation of the proj1 object creates a room. Arguments include the x and y dimensions, and the number of people to place on the board. The __init__ function then creates the board and places the people on it randomly.\n\nThe function in the proj1 class called 'update' updates the board by a certain number of steps. In the creation of the people, there is a specifiable parameter for maximum velocity, which means some people can move quicker if that is the desire. Th update function moves everyone in random directions and updates the board.\n\nThe function in the student class '__rand_exp' is vary similar (if not identical) to how numpy draws a random exponential distribution. A scaling factor is specifiable as is in numpy.")
	
	dct = {"yes":"yes", "ye":"yes", "y":"yes", "no":"no", "n":"no"}
	default = "y"
	options = ["y","n"]
	run = menu()
	if run.query_options("Do you want me to run a 'simulation'? (100 iterations of a 50x50 board with 100 people on it)",dct,options,default)=="yes":
		xdim=50
		ydim=50
		nump=100
		it=100
		test=proj1(xdim,ydim,nump,1)
		test.print_board()
		for x in range(it):
			time.sleep(0.25)
			test.update()
			test.print_board()
	else:
		print("Ok, you can just look in my code then")

if __name__=="__main__":
	main()
