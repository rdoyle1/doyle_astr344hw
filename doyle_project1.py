import sys
import numpy as np
import time
import random
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

"""
Project class. Constructor takes the following optional arguments:
	xdim - x dimension of board
	ydim - y dimension of board
	num_people - number of individuals placed on the board
	vel - maximum velocity of the individuals
	movement - type of movement.
		[0] is randome movement
		[1] is directional movement
"""
class proj1:
	def __init__(self,xdim=100,ydim=100,num_people=100,vel=2,movement=0):
		self.x=xdim
		self.y=ydim
		self.num_people=num_people
		self.max_vel=vel
		self.grid = [[-1 for x in range(self.x)] for y in range(self.y)]
		self.people=[]
		self.movement=movement
		for i in range(num_people):
			while True:
				xloc=int(np.random.random()*xdim)
				yloc=int(np.random.random()*ydim)
				if self.grid[yloc][xloc]==-1:
					self.grid[yloc][xloc]=i
					self.people.append(student(xloc,yloc,i,vel,self.people,xdim,ydim))
					break

		self.time=0
		self.has_run=False
	
	"""
	Update the game board, taking into account the number of
	steps to take in the update, and the personal space bubble
	(if desired) of the individuals
	"""
	def update(self,steps=1,personal_space=0):
		self.temp_grid = [[-1 for x in range(self.x)] for y in range(self.y)]
		random.shuffle(self.people)
		for person in self.people:
			person.update(steps,self.movement,personal_space)
			x,y=person.get_pos()
			id_num=person.get_id()
			self.temp_grid[y][x]=id_num
		self.grid=self.temp_grid
		for person in self.people:
			person.check_sickness(self.time)
		return

	"""
	Print the stats of the run - iteration (time step), the number
	of sick and vaccinated people.
	"""
	def print_stats(self):
		print("\033[KIteration {}, {} people, {} vaccinated, {} sick".format(self.time,self.num_people,self.get_vaccinated(),self.get_infected()))
	
	"""
	run a single simulation. optional arguments are:
		num_sick - the initial number of sick individuals on the board
		num_vaccinated - the initial number of vaccinated individuals
		personal_space - the personal space buffer of the individuals
		iterations - the maximum number of iterations to run to try to
			infect everyone. This is to prevent infinite corner cases
		timestep - time to sleep in between updates (only applies if we
			are printing the board.
		printing - do we want to print the board after each iteration?
		print_stats - should we print the statistics of the run after it
			finishes?
	"""
	def run(self,num_sick=1,num_vaccinated=0,personal_space=0,iterations=100,timestep=1,printing=True,print_stats=True):
		if self.has_run:
			self.__init__(self.x,self.y,self.num_people,self.max_vel)
		self.time=0
		if num_sick+num_vaccinated>self.num_people:
			print("The total number of people vaccinated and sick must be less than the number of people")
			raise ValueError
		for i in range(num_sick):
			while True:
				person=random.choice(self.people)
				if not person.is_sick():
					person.make_sick(0)
					break
		for i in range(num_vaccinated):
			while True:
				person=random.choice(self.people)
				if not person.is_sick() and not person.is_vaccinated():
					person.vaccinate()
					break
		if printing:
			self.print_board()
			time.sleep(timestep)
		for i in range(iterations):
			self.update(1,personal_space)
			num_sick=self.get_infected()
			num_vacc=self.get_vaccinated()
			self.time+=1
			if printing:
				self.print_board()
				time.sleep(timestep)
			if num_sick+num_vacc>=self.num_people:
				break
		if print_stats:
			self.print_stats()
		self.has_run=True
		data = [p.time_infected() for p in self.people if p.is_sick()]
		return self.time,data

	"""
	return a total number of vaccinated people in the room
	"""
	def get_vaccinated(self):
		num_vacc=0
		for person in self.people:
			if person.is_vaccinated():
				num_vacc+=1
		return num_vacc

	"""
	return the total number of infected people in the room
	"""
	def get_infected(self):
		num_sick=0
		for person in self.people:
			if person.is_sick():
				num_sick+=1
		return num_sick

	"""
	Print the room on the screen. 
	"""
	def print_board(self):
		people=[x.get_id() for x in self.people]
		print("\033[0;0H\033[K"+"-"*(self.x*3+1))
		for i,row in enumerate(self.grid):
			col=2
			print("\033[{};0H\033[K|".format(i+2))
			for person in row:
				if person==-1:
					print("\033[{};{}H   ".format(i+2,col))
				elif self.people[people.index(person)].is_sick():
					print("\033[{};{}H x ".format(i+2,col))
				elif self.people[people.index(person)].is_vaccinated():
					print("\033[{};{}H o ".format(i+2,col))
				else:
					print("\033[{};{}H * ".format(i+2,col))
				col+=3
			print("\033[{};{}H|".format(i+2,col))
		print("\033[{};0H\033[K".format(self.x+2)+"-"*(self.x*3+1))
		self.print_stats()

"""
student "person" class. Arguments are:
	x,y - x ansd y positions on the board of the individual
	id_num - id of the student
	max_vel - maximum velocity of person
	room - list of all people in the room
	max_x, max_y - the dimensions of the board
"""
class student:
	def __init__(self,x,y,id_num,max_vel,room,max_x,max_y):
		self.x=x
		self.y=y
		self.id_num=id_num
		self.room=room
		self.max_vel=max_vel
		self.vel=self.__pick_rand_vel()
		self.max_x=max_x
		self.max_y=max_y
		self.sick=False
		self.when_infected=0
		self.vaccinated=False
		self.direction=self.__pick_rand_dir()

	"""
	pick a random direction
	"""
	def __pick_rand_dir(self,maxdist=1):
		options = [(x,y) for x in range(self.x-maxdist,self.x+maxdist+1) for y in range(self.y-maxdist,self.y+maxdist+1) if x>=0 and x<self.max_x and y>=0 and y<self.max_y and x!=self.x and y!=self.y and ((np.abs(x-self.x)==maxdist and np.abs(y-self.y)==0) or (np.abs(x-self.x)==0 and np.abs(y-self.y)==maxdist) or (np.abs(x-self.x)==maxdist and np.abs(y-self.y)==maxdist))]
		people = [x.get_pos() for x in self.room if x.get_id()!=self.id_num]
		spaces = list(set(options)-set(people))
		dirs = [((x-self.x)/maxdist, (y-self.y)/maxdist) for x,y in spaces]
		if len(dirs)>0:
			return random.choice(dirs)
		else:
			return (0,0)

	"""
	pick a random direction
	"""
	def __pick_rand_vel(self):
		if self.max_vel<=1:
			return 1
		else:
			return random.choice(range(1,self.max_vel+1))

	"""
	update the person. Pick a new location on the board, check if you got sick
	"""
	def update(self,steps=1,updatestyle=0,personal_space=0):
		if updatestyle==0:
			fvel=(np.log(np.random.random()+1)/np.log(self.max_vel+1))*self.max_vel
			vel=int(round(fvel,0))
			options = [(x,y) for x in range(self.x-vel,self.x+vel+1) for y in range(self.y-vel,self.y+vel+1) if x>=0 and x<self.max_x and y>=0 and y<self.max_y]
			people = [x.get_pos() for x in self.room if x.get_id()!=self.id_num]
			positions = list(set(options)-set(people))
			self.x,self.y = random.choice(positions)
		else:
			dx,dy = self.direction
			vel = self.vel
			if self.vel==(0,0) or self.__conflicts(self.x+(dx*vel),self.y+(dy*vel),personal_space):
				self.vel = self.__pick_rand_vel()
				self.direction = self.__pick_rand_dir(self.vel)
			else:
				self.x += (dx*vel)
				self.y += (dy*vel)
					
	"""
	check if the chosen location conflicts with another person or restriction
	"""
	def __conflicts(self,x,y,personal_space=0):
		if len(set([(x,y)])&set([person.get_pos() for person in self.room if person.get_id()!=self.id_num]))>0:
			return True
		elif x<0 or x>=self.max_x or y<0 or y>=self.max_y:
			return True
		elif personal_space!=0:
			options = set([(x1,y1) for x1 in range(x-personal_space,x+personal_space+1) for y1 in range(y-personal_space,y+personal_space+1) if x>=0 and x<self.max_x and y>=0 and y<self.max_y])
			people = set([person.get_pos() for person in self.room if person.get_id()!=self.id_num])
			if len(options&people)>0:
				return self.__rand_exp()<0.75
			else:
				return False
		else:
			return False
		#for person in self.room:
		#	if person.get_id()!=self.id_num:
		#		xpos,ypos=person.get_pos()
		#		if x==xpos and y==ypos:
		#			return True
		#return False

	"""
	get the position of the person
	"""
	def get_pos(self):
		return self.x,self.y
	
	"""
	get the id of the person
	"""
	def get_id(self):
		return self.id_num
	
	"""
	check if the person is sick
	"""
	def is_sick(self):
		return self.sick
	
	"""
	get the time we were infected
	"""
	def time_infected(self):
		return self.when_infected
	
	"""
	make us sick
	"""
	def make_sick(self,timestep):
		self.sick=True
		self.when_infected=timestep
		return
	
	"""
	vaccinate us
	"""
	def vaccinate(self):
		self.vaccinated=True
	
	"""
	check if we're vaccinated
	"""
	def is_vaccinated(self):
		return self.vaccinated
	
	"""
	draw a random exponential
	"""
	def __rand_exp(self,scale=1.0):
		return -scale*(np.log(scale*np.random.random())-np.log(scale))
	
	"""
	check if we get infected
	"""
	def __infected(self,dist=1.0):
		if not self.vaccinated:
			return self.__rand_exp(dist**2) < 0.75
			#return np.random.exponential(0.25) < 0.5
		else:
			return False
	
	"""
	get the distance from a location on the board
	"""
	def __dist(self,x,y):
		return np.sqrt((x-self.x)**2+(y-self.y)**2)

	"""
	check if we got sick in the update based on location
	"""
	def check_sickness(self,timestep):
		if self.sick:
			return
		people = [x.get_pos() for x in self.room]
		sick = [x.is_sick() for x in self.room]
		positions = [(x,y) for x in range(self.x-2,self.x+3) for y in range(self.y-2,self.y+3) if x>=0 and x<self.max_x and y>=0 and y<self.max_y and x!=self.x and y!=self.y]
		matches = set(positions).intersection(set(people))
		for x,y in matches:
			if sick[people.index((x,y))]:
				if self.__infected(self.__dist(x,y)):
					self.make_sick(timestep)
					return


def closest_ind(x,vals):
	mins = [np.abs(y-x) for y in vals]
	return mins.index(min(mins))

def get_input(val):
	try:
		inpt=raw_input(val)
	except NameError:
		inpt=input(val)
	return inpt

def main():
	"""xdim=int(get_input("x-dim >>> "))
	ydim=int(get_input("y-dim >>> "))
	nump=int(get_input("num people >>> "))
	sic=int(get_input("num infected >>> "))
	vac=int(get_input("num vaccinated >>> "))
	it=int(get_input("iterations >>> "))
	tg=int(get_input("num tests >>> "))
	test=proj1(xdim,ydim,nump)
	times=[]
	tests=tg
	for i in range(tests):
		print("\033[2J\033[0;0HIteration {}".format(i))
		times.append(test.run(sic,vac,0,it,0.25,False,False))
	vals=np.linspace(min(times),max(times),min(int(np.sqrt(tests))+1,max(times)-min(times)+1))
	ys=np.zeros(min(int(np.sqrt(tests))+1,max(times)-min(times)+1)) 
	for t in times:
		ys[closest_ind(t,vals)]+=1
	plt.plot(vals,ys)                                       
	plt.show() 
	"""
	
	nump=40
	num_iter=100
	test=proj1(20,20,nump,vel=1,movement=1)
	test.run(1,0,0,10000,0.25,True,False)
	"""
	chart={}
	for i in range(nump):
		for j in range(nump):
			if j==0:
				continue
			if i+j<=nump:
				print("\033[2J\033[1;1HSick {}, vacc {}".format(j,i))
				for z in range(num_iter):
					print("\033[2;1H\033[KIteration {}".format(z))
					if "{} {}".format(i,j) in chart:
						chart["{} {}".format(i,j)].append(test.run(j,i,0,10000,0.25,False,False))
					else:
						chart["{} {}".format(i,j)]=[test.run(j,i,0,10000,0.25,False,False)]
	for item in chart:
		avg=sum(chart[item])/(len(chart[item])*1.0)
		chart[item]=avg
	infected=[]
	vaccinated=[]
	average=[0]*(nump**2)
	for i in range(nump):
		infected+=[i]*nump
		for j in range(nump):
			vaccinated+=[j]
	for i in range(nump):
		for j in range(nump):
			if '{} {}'.format(j,i) in chart:
				average[i*nump+j]=chart['{} {}'.format(j,i)]
	numdel=0
	for x,y,z,i in zip(infected,vaccinated,average,range(nump**2)):
		if z==0:
			del infected[i-numdel]
			del vaccinated[i-numdel]
			del average[i-numdel]
			numdel+=1
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.scatter(infected, vaccinated, average)
	ax.set_xlabel('Number Initially Infected')
	ax.set_ylabel('Number Initially Vaccinated')
	ax.set_zlabel('Number of Iterations')
	plt.show()
	"""

if __name__=="__main__":
	main()
