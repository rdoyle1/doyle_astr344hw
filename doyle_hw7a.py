import numpy.random as random
import numpy as np
import time

class hw7:
	def __init__(self,f):
		self.f=f
		self.random_seed=int(time.time())
	
	def seed(self,x=None):
		if x==None:
			self.random_seed=time.time()
		else:
			self.random_seed=x

	def rand(self):
		self.random_seed=self.random_seed*1103515245+12345
		if self.random_seed<0:
			self.random_seed*=-1
		return (self.random_seed/65536)%32768

	def __dist(self,p1,p2):
		return np.sqrt((p2[0]-p1[0])**2+(p2[1]-p1[1])**2)

	def calc_pi(self,num_points=10000):
		radius=1.0
		tot=0
		for i in range(num_points):
			x=(random.random()*2*radius)-1
			y=(random.random()*2*radius)-1
			if self.__dist([0,0],[x,y])<=radius:
				tot+=1
		area=((2.0*radius)**2)*(1.0*tot)/num_points
		return area/(radius**2)

	def find_bday_match(self,trials=10000):
		num_people=0
		prob=0.0
		while prob<0.5:
			num_people+=1
			tot=0
			for i in range(trials):
				bdays = [int(random.random()*365) for x in range(num_people)]
				same = set(bdays)
				if len(bdays)!=len(same):
					tot+=1
			prob=(1.0*tot)/trials
			print("Testing on {} people. {} trials, {} matched birthdays (probability of {})".format(num_people,trials,tot,prob))
		return num_people

test=hw7(None)
print("Calculating pi:")
print("Here, we calculate pi by taking a circle of radius 1, centering it at (0,0), and use monte-carlo\nmethod to find the area. Then, we simply use a=pi*r^2 to find what pi is.\nAs we increase the number of random points generated, the accuracy\nof the estimation increases.\n")
print("Using 100 points: pi = {}".format(test.calc_pi(100)))
print("1,000 points: pi = {}".format(test.calc_pi(1000)))
print("10,000 points: pi = {}".format(test.calc_pi(10000)))
print("100,000 points: pi = {}".format(test.calc_pi(100000)))
print("1,000,000 points: pi = {}".format(test.calc_pi(1000000)))
print("As you can see, as we increase the number of trial points, the accuracy of the\narea estimation for the circle increases, and so does the estimation\nfor pi as a result.")
