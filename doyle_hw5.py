#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import random

def f(x):
	return np.sin(x)

def g(x):
	return (x**3)-x-2

def y(x):
	return -6+x+(x**2)

class hw5:
	def __init__(self,function=None,limits=None,tolerance=1.48e-8,max_iterations=10):
		if function==None:
			self.f=self.__default_func
			self.limits=[0,0]
		else:
			self.f=function
			self.limits=limits
		self.cur_limits=self.limits[:]
		self.min_limits=self.limits[:]
		self.max_limits=self.limits[:]
		self.tolerance=tolerance
		self.iterations=max_iterations
		self.print_steps=True
	
	def __default_func(x):
		return 0

	def change_tolerance(self,tolerance):
		self.tolerance=tolerance

	def change_function(self,func,lim):
		self.f=func
		self.limits=lim
		self.cur_limits=self.limits[:]
		self.min_limits=self.limits[:]
		self.max_limits=self.limits[:]

	def find_root(self,iterations=10,limit=True,print_steps=True):
		self.iterations=iterations
		self.print_steps=print_steps
		return self.__run(iterations,limit,False)

	def __print(self,statement):
		if self.print_steps:
			print(statement)

	def __run(self,iterations=10,limit=True,method=False):
		# find values of the function at the endpoints
		left=self.f(self.cur_limits[0])
		right=self.f(self.cur_limits[1])
		# check if there is or is not a root within the values
		if left*right>0:
			curl,curr=(self.cur_limits[0],self.cur_limits[1])
			if method:
				self.cur_limits[0]*=random.uniform(1.,1.1)
				self.cur_limits[1]*=random.uniform(1.,1.1)
				self.__print("No root apparent from [{}, {}]. Extending range to [{}, {}]".format(curl,curr,self.cur_limits[0],self.cur_limits[1]))
			else:
				self.cur_limits[0]/=random.uniform(1.,1.1)
				self.cur_limits[1]/=random.uniform(1.,1.1)
				self.__print("No root apparent from [{}, {}]. Shrinking range to [{}, {}]".format(curl,curr,self.cur_limits[0],self.cur_limits[1]))
			if iterations<=0:
				if limit and method:
					self.__print("Limiting is enabled - No root found with current limits")
					return 0
				elif not limit and method:
					self.__print("Limiting is disabled - Increasing the number of iterations")
					self.iterations*=2
				if method:
					self.__print("Setting limits from [{}, {}] to [{}, {}]".format(self.cur_limits[0],self.cur_limits[1],self.min_limits[0],self.min_limits[1]))
					self.max_limits=self.cur_limits[:]
					self.cur_limits=self.min_limits[:]
				else:
					self.__print("Setting limits from [{}, {}] to [{}, {}]".format(self.cur_limits[0],self.cur_limits[1],self.max_limits[0],self.max_limits[1]))
					self.min_limits=self.cur_limits[:]
					self.cur_limits=self.max_limits[:]
				#self.cur_limits=self.limits[:]
				
				return self.__run(self.iterations,limit,~method)
			else:
				return self.__run(iterations-1,limit,method)
		return self.__rec(self.cur_limits[0],self.cur_limits[1])

	"""
	Recursive function that finds the root by bisection
	"""
	def __rec(self,l_lim,r_lim):
		# find the value of the function at the endpoints and midpoint.
		left=self.f(l_lim)
		mid=self.f((r_lim+l_lim)/2.)
		right=self.f(r_lim)
		# either recurse more or return the point that is within tolerance
		if np.abs(mid)<self.tolerance:
			self.__print("x={} is within tolerance. Returning...".format((r_lim+l_lim)/2.))
			return (r_lim+l_lim)/2.
		elif left*mid<0:
			self.__print("Root is in the range [{}, {}]. Recursing on range...".format(l_lim,(r_lim+l_lim)/2.))
			return self.__rec(l_lim,(r_lim+l_lim)/2.)
		elif right*mid<0:
			self.__print("Root is in the range [{}, {}]. Recursing on range...".format((r_lim+l_lim)/2.,r_lim))
			return self.__rec((r_lim+l_lim)/2.,r_lim)
		else:
			if left==0:
				self.__print("x={} is within tolerance. Returning...".format(l_lim))
				return l_lim
			else:
				self.__print("x={} is within tolerance. Returning...".format(r_lim))
				return r_lim
	
	def get_limits(self):
		return self.limits


if __name__=="__main__":
	print("The toggles you asked for are all options and functions available when using the code. For example, to change the limits, you use the change_limits function. To change the tolerance, you use change_tolerance. To change the function you are finding the roots for, you use change_function. The function find_root calls the private recursive bisecting function, and I'm having the functions print what they're doing for clarity while you're grading. When find_root is called, there are two optional arguments. The first is the number of iterations you want to do. The second - \"limit\" - is if we want to limit the root finder to that many limit-checking iterations.\n")
	print("Running on function f(x)=sin(x). Root should be pi.")
	test=hw5(f,[2,4]) # initialize the class object using the function f, with limits 2 to 4.
	lims=test.get_limits()
	print("Root of f between {} and {}: x={}\n".format(lims[0],lims[1],test.find_root())) # Find the roots within the bounds

	print("Running on function g(x)=x^3-x-2. Root should be 1.521...")
	test.change_function(g,[1,2]) # Change the function to be g(x) with limits 1 to 2
	lims=test.get_limits()
	print("Root of g between {} and {}: x={}\n".format(lims[0],lims[1],test.find_root())) # find and print the root

	print("Running on function y(x)=x^2+x-6. Root should be 2.")
	test.change_function(y,[0,5]) # Change the function to be y(x) with limits 0 to 5
	lims=test.get_limits()
	print("Root of y between {} and {}: x={}\n".format(lims[0],lims[1],test.find_root(limit=False))) # find the roots
