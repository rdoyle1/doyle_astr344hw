Randy Doyle's computational astro repo

Homework:

- Homework 2 - precision of floating points.

- Homework 3 - simple differentiation

- Homework 4 - simple integration

- Homework 5 - simple root finding

- Homework 6 - NR root rinding and timing

- Homework 7 - Monte Carlo stuff

Projects:

- none yet
