#!/usr/bin/python
import numpy as np
import math

recur32=[]  #single precision list
recur64=[]  #double precision list

"""
reset the lists to their initial values (user specifiable)
"""
def reset_lists(n0=1,n1=1):
	global recur32, recur64
	recur32=[None for x in range(100)]
	recur32[0]=np.float32(n0)
	recur32[1]=np.float32(n1)
	
	recur64=[None for x in range(100)]
	recur64[0]=np.float64(n0)
	recur64[1]=np.float64(n1)

"""
Calculate the recurrence with single precision
"""
def recurrence_32(n):
	global recur32
	if recur32[n]!=None:
		return recur32[n]
	else:
		val = (13.0/3)*recurrence_32(n-1)-(4.0/3)*recurrence_32(n-2)
		recur32[n]=np.float32(val)
		return recur32[n]

"""
Calculate the recurrence with double precision
"""
def recurrence_64(n):
        global recur64
        if recur64[n]!=None:
                return recur64[n]
        else:
		val = (13.0/3)*recurrence_64(n-1)-(4.0/3)*recurrence_64(n-2)
                recur64[n]=np.float64(val)
                return recur64[n]

reset_lists(1,1.0/3) #reset the lists, make n_0=1 and n_1=1/3

print("Part 1\n")

for n in range(21): #for n=0 to n=5, calculate all the desired values
	sp = recurrence_32(n)
	fp = recurrence_64(n)
	alt32 = np.float32((1.0/3)**n)
	alt64 = np.float64((1.0/3)**n)
	print("(n={0}) using recursion relation - single: {1:.5g}, double: {2:.5g}, rel err = {3:.5g}%, abs err = {4:.5g}".format(n,sp,fp,(math.fabs(fp-sp)/fp)*100,fp-sp))
	print("      using (1/3)^n directly   - single:  {0:.5g}, double: {1:.5g}, rel err = {2:.5g}%, abs err = {3:.5g}".format(alt32,alt64,(math.fabs(alt64-alt32)/alt64)*100,alt64-alt32))
	print("")

#calculate the n=20 value to show how crazy this gets with single precision
print("")
print("\nSo I did this 2 ways. For each n, I did a simple dynamic programming tabulation exercise. Note that due to problems with the rounding of floating points (it only keeps 7 decimal points for single precision and 15 for double), as we iterate over the recursion relation using the previously calculated and stored values, the result gets progressively more incorrect. Using the equation 1/3^n directly, we get around that, because we aren't storing rounded values as we progress.\n")
print("This gets prety crazy, because the single point float only tracks 7 decimal points. As soon as we go smaller than this, the computer tries to interpret the number, but bits have gotten flipped by our calculations in a way the computer doesn't understantd. The computer reads the number as it usually does, but at this point, it's too late, and the number is completely wrong.\n")


reset_lists(1,4)

print("Part 2: \n")
for n in range(21): #for n=0 to n=5, calculate all the desired values
        sp = recurrence_32(n)
        fp = recurrence_64(n)
        alt32 = np.float32((4)**n)
        alt64 = np.float64((4)**n)
        print("(n={0}) using recursion relation - single: {1:.5g}, double: {2:.5g}, rel err = {3:.5g}%, abs err = {4:.5g}".format(n,sp,fp,(math.fabs(fp-sp)/fp)*100,fp-sp))
        print("         using (4)^n directly   - single:  {0:.5g}, double: {1:.5g}, rel err = {2:.5g}%, abs err = {3:.5g}".format(alt32,alt64,(math.fabs(alt64-alt32)/alt64)*100,alt64-alt32))
        print("")
print("")
print("This relation is stable, because it is growing larger, not going to zero. We don't have to worry about too many decimal places like the last one, and we also don't have to worry about overflow (too large a number) because we haven't gotten too large for a single precision number yet.")
