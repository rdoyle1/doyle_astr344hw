#!/usr/bin/python
import numpy as np
import matplotlib.pyplot as plt
import sys

def derive_dN_dL(N,L):
	dNdL=[]
	Ll=[]
	length = len(N)
	for (i,x),y in zip(enumerate(N),L):
		if i<length-1:
			dNdL.append(np.log10(-1*(N[i+1]-N[i])/(L[i+1]-L[i])))
			Ll.append(np.log10((L[i+1]+L[i])/2))
	return Ll,dNdL

L=[]
N=[]
logL=[]
dNdL=[]

print("I'm not sure exactly what format you want the I/O, so I'll have you enter the file path for both the model_smg file and the ncounts_850 file.\n")
smgfile = raw_input("Enter smg filename: ").strip()
# my computer has it at: ../astr344_homework_materials/model_smg.dat
with open(smgfile) as f:
	for line in f:
		line = line.strip().split()
		L.append(float(line[0]))
		N.append(float(line[1]))

ncountfile = raw_input("Enter ncount filename: ").strip()
# my computer has it at: ../astr344_homework_materials/ncounts_850.dat
with open(ncountfile) as f:
	for line in f:
		line = line.strip().split()
		logL.append(float(line[0]))
		dNdL.append(float(line[1]))

model_logL,model_dNdL = derive_dN_dL(N,L)
plt.plot(logL,dNdL,'ro',model_logL,model_dNdL,'b-o',linewidth=5.0)
plt.show()
