#!/usr/bin/python
"""
Randy Doyle
Homework 4
9/29/2015
"""
import numpy as np
import matplotlib.pyplot as plt

"""
Calculate the required values, and returns both the value that we calculate
for that z value and the value np.trapz calculates.
"""
def calculate(z,numsteps=100):
	if z<=0:
		return 0,0 # we can't do linspace on an interval from 0 to 0, so just return 0.
	x=np.linspace(0,z,int(numsteps*z))
	y=[(3.0e3)/hubble(val) for val in x] # make a list of the y values.
	return integrate(x,y)/(1.+z),np.trapz(y,x)/(1.+z)

"""
Test function seeing what the difference between lists and maps are
Only used this when trying to figure out why some people were getting
1700 as a maximum.
"""
def runmap(z,numsteps=100):
	x=np.linspace(0,z,int(numsteps*z))
	mapx=x.tolist()
	maptemp=map(hubble,mapx)
	mapy=[3.e3/val for val in maptemp]
	return integrate(mapx,mapy)/(1.+z),np.trapz(mapy,mapx)/(1.+z)

"""
returns the H(z) value defined on the homework sheet
"""
def hubble(z):
	return np.sqrt((0.3*(1.+z)**3)+(0*(1.+z)**2)+0.7)

"""
Integration function - x is a list of x values, y is a list of corresponding y values.
Calculates result using a trapezoidal method.
"""
def integrate(x,y):
	xlen=len(x)
	a=[]
	for i in range(len(x)): # for every trapezoid in our range, calculate the area.
		if i<xlen-1:
			dx=x[i+1]-x[i] # calculate the dx
			h=(y[i+1]+y[i])/2.0 # calculate the trapezoid height
			a.append(dx*h) #multiply them together and add onto the sum
	return sum(a) #sum the list of trapezoid areas to get final value

def f(x):
	return np.sin(x)

print("Blue line calculated by trapz, red dots are mine.")
print("A bit more explanation of how it works is in the code comments")
x=np.linspace(0,10,100)
ret=[calculate(z) for z in x]
y1=[a for a,b in ret]
y2=[b for a,b in ret]
#ret1=[runmap(z) for z in x]
#y11=[a for a,b in ret1]
#y21=[b for a,b in ret1]
plt.plot(x,y1,'ro',x,y2,'b-.',linewidth=4.)#x,y11,'go',x,y21,'m-.',linewidth=4.)
plt.show()
