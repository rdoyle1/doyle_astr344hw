import numpy as np
import matplotlib.pyplot as plt

def is_float(val):
	try:
		float(val)
	except ValueError:
		return False
	return True

def get_input(val):
	try:
		inpt = raw_input(val)
	except NameError:
		inpt = input(val)
	return inpt

def avg(vals):
	return (1.0*sum(vals))/len(vals)

def element_multiply(x,y):
	return [x*y for x,y in zip(x,y)]

def least_squares(x, y):
	a1 = (sum(element_multiply(x,y))-len(x)*avg(x)*avg(y))/(sum(element_multiply(x,x))-len(x)*(avg(x)**2))
	a2 = (avg(y)*sum(element_multiply(x,x))-avg(x)*sum(element_multiply(x,y)))/(sum(element_multiply(x,x))-len(x)*(avg(x)**2))
	return a1,a2

def f(x,a1,a2):
	return a1*x+a2

def main():
	x = []
	y = []
	path = get_input("Where is the data file located? (its path)\n>>> ").strip()
	with open(path) as fp:
		for line in fp:
			line = line.strip().split()
			if is_float(line[0]):
				x.append(np.log10(float(line[0])))
			if is_float(line[1]):
				y.append(np.log10(float(line[1])))
	a1,a2 = least_squares(x,y)
	print("Fit: log10(star formation rate) = {} log10(CO brightness) + {}".format(a1, a2))
	y2 = [f(z,a1,a2) for z in x]
	plt.plot(x,y,'ro',x,y2,'b-')
	plt.show()

if __name__=="__main__":
	main()
